// add purple number through css

class MyHandler extends Paged.Handler {
    constructor(chunker, polisher, caller) {
        super(chunker, polisher, caller);
    }

    afterParsed(content) {
        content.querySelectorAll("p").forEach((element, i) => {
            element.setAttribute("data-num", i+1);
        })
    }
}

Paged.registerHandlers(MyHandler);

// class linkBack extends Paged.Handler {
//     constructor(chunker, polisher, caller) {
//       super(chunker, polisher, caller);
//     }
  
//     beforeParsed(content) {
//         resetChapterNotesNumbering()
//     }
//   }
  
//   Paged.registerHandlers(linkBack);
  
  
//   function resetChapterNotesNumbering() {
//     const sections = content.querySelectorAll("section");
//     let j = 0;
//     sections.forEach((section, sectionIndex) => {
//       j++;
//       const noteCallouts = section.querySelectorAll(".inline-note-callout");
//       noteCallouts.forEach((noteCallout, calloutIndex) => {
//         noteCallout.setAttribute("href", `#note-ch${sectionIndex}-n${calloutIndex}`);
//         noteCallout.setAttribute("id", `callout-ch${j}-en${calloutIndex + 1}`);
//       });
  
//       const notes = section.querySelectorAll(".notes li");
//       notes.forEach((note, noteIndex) => {
//         note.id = `note-ch${sectionIndex}-n${noteIndex}`;
    
//         note.insertAdjacentHTML("beforeend", `<a class="link-back" href="#callout-ch${j}-en${noteIndex + 1}"> ⤶ </a>`);
//         console.log("note");
//       });
//     });
//   }
  